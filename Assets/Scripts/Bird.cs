﻿using UnityEngine;

public class Bird : MonoBehaviour
{
    public float upForce = 200f;

    private bool isDead = false;

    private Rigidbody2D _rigidbody2D;
    private Animator _animator;

    // Use this for initialization
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead == false)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _rigidbody2D.velocity = Vector2.zero;
                _rigidbody2D.AddForce(new Vector2(0, upForce));
                _animator.SetTrigger("Flap");
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        _rigidbody2D.velocity = Vector2.zero;
        isDead = true;
        _animator.SetTrigger("Die");
        GameController.instance.BirdDie();//Call static instance
    }
}