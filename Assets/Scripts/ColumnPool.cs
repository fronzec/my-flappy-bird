﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class ColumnPool : MonoBehaviour
{
    public int columnPoolSize = 0;
    public float spawnRate = 4f;
    public GameObject columnPrefab;
    private GameObject[] columns;
    private Vector2 objectPoolPosition = new Vector2(-15f, -25f); //Offscreen position
    private float spawnXPosition = 10f;
    public float columnMin = -1f;
    public float columnMax = 3.5f;


    private float timeSinceLastSpawn;

    private int currentColumn = 0;

    // Use this for initialization
    void Start()
    {
        columns = new GameObject[columnPoolSize];
        for (int i = 0; i < columnPoolSize; i++)
        {
            columns[i] = (GameObject) Instantiate(columnPrefab, objectPoolPosition, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceLastSpawn += Time.deltaTime;
        if (GameController.instance.gameOver == false && timeSinceLastSpawn >= spawnRate)
        {
            timeSinceLastSpawn = 0;
            float spawnYPosition = Random.Range(columnMin, columnMax);
            columns[currentColumn].transform.position = new Vector2(spawnXPosition, spawnYPosition);
            currentColumn++;
            if (currentColumn % columnPoolSize == 0)
            {
                currentColumn = 0;
            }
        }
    }
}