﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

	public static GameController instance;
	public GameObject gameOverText;
	public Text ScoreText;
	public bool gameOver = false;
	public float scrollSpeed = -1.5f;
	private int score = 0;
	void Awake () {
		if (instance == null)
		{
			instance = this;
		}	else if (instance != this)
		{
			Destroy(gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (gameOver == true && Input.GetMouseButtonDown(0))
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
	}


	public void BirdScore()
	{
		if (gameOver)
		{
			return;
		}
		score++;
		ScoreText.text = "Socore : " + score.ToString();
	}
	public void BirdDie()
	{
		gameOverText.SetActive(true);
		gameOver = true;
	}
}
