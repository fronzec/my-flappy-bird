﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackground : MonoBehaviour
{

	private BoxCollider2D _boxCollider2D;
	private float groundHorizontalLenght;
	
	
	// Use this for initialization
	void Start ()
	{
		_boxCollider2D = GetComponent<BoxCollider2D>();
		groundHorizontalLenght = _boxCollider2D.size.x;
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < -groundHorizontalLenght)
		{
			RepositionBackground();
		}
	}

	private void RepositionBackground()
	{
		Vector2 groundOffset = new Vector2(groundHorizontalLenght * 2f, 0);
		transform.position = (Vector2) transform.position + groundOffset;
	}
}
