﻿using UnityEngine;

public class ScrollingObject : MonoBehaviour
{
    private Rigidbody2D _rigidbody2d;

    // Use this for initialization
    void Start()
    {
        _rigidbody2d = GetComponent<Rigidbody2D>();
        _rigidbody2d.velocity = new Vector2(GameController.instance.scrollSpeed, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.instance.gameOver == true)
        {
            _rigidbody2d.velocity = Vector2.zero;
        }
    }
}